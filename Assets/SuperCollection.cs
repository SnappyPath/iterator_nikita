﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperCollection : IEnumerable
{
    private string[] items;

    public string[] GetItems
    {
        get
        {
            return items;
        }
    }


    public SuperCollection(string[] values)
    {
        items = values;
    }

    public IEnumerator GetEnumerator()
    {
        return new SuperCollectionEnumerator(this);
    }



}
