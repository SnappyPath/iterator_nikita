﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;

public class SuperCollectionEnumerator : IEnumerator
{
    private SuperCollection collection;

    private int Pos = -1;

    public SuperCollectionEnumerator(SuperCollection st)
    {
        collection = st;
    }

    public object Current
    {
        get
        {
            return collection.GetItems.OrderBy(c => c.Length).ToArray()[Pos];
        } 

    }

    public bool MoveNext()
    {
        Debug.Log(Pos);
        Debug.Log(collection.GetItems);
        if (Pos < collection.GetItems.Length)
        {
            Pos++;
            //return true;
        }

        return Pos < collection.GetItems.Length;
        
    }

    public void Reset()
    {
        Pos = -1;
    }
 




}
